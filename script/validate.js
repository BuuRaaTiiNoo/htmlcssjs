$(document).ready(function () {


    function Person(fname, lname, login, password) {
        this.fname = fname;
        this.lname = lname;
        this.login = login;
        this.password = password;
    }

    $(".freg").submit(function (event) {
        let person = new Person($(".freg #fname").val(), $(".freg #lname").val(), $(".freg #login").val(), $(".freg #password").val());

        if (person.login != "root") {
            return;
        } else
            if ($(".message").html() === "")
                $(".message").append("<h5>Пользователь с таким именем уже существует</h5>");
        event.preventDefault();
    });

    $(".flogin").submit(function (event) {
        let login = $(".flogin #login").val();
        let password = $(".flogin #password").val();

        if (login === "root" && password === "root") {
            return;
        } else
            if ($(".message").html() === "")
                $(".message").append("<h5>Неверный логин или пароль</h5>");
        event.preventDefault();
    });
});
