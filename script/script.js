$(document).ready(function () {

    const value = $(".p-1").css("width");
    
    //search
    let isResizeble = false;
    let val = null;
    $("#searchbtn").click(function () {
        let product;
        let content = $(".bodycontent").children().children();
        val = $("#searchform").val().toLowerCase();

        if (!isResizeble) {
            $(".content .p-1").each(function () {
                product = $(this);
                content.append(product.html());
            });
            isResizeble = true;
        }

        $(".bodycontent").find(".card-body").removeClass("d-flex justify-content-between bg-default mb-3");
        $(".bodycontent").find(".card-body").children().removeClass("p-2 flex-fill bg-default");
        $(".bodycontent").find(".card-body").children().css({ "display": "block" });
        $(".bodycontent").find(".description").css("display", "none");
        $(".bodycontent .p-1").css({ "width": value });

        $(".bodycontent .card-text").filter(
            function () {
                $(this.parentNode.parentNode.parentNode).toggle($(this.parentNode.parentNode.parentNode).text().toLowerCase().indexOf(val) > -1)
            })
    });

    //collapse
    $(".card-body").click(function () {
        //Устанавливаем ширину 100%
        $(this.parentNode.parentNode).css({ "width": "100%" });

        //Отображаем скрытое поле description, выстраиваем элементы.
        $(this).children().css({ "display": "inline" });
        $(this).children().children().css({ "display": "inline" });

        //Добавление классов к элементам
        $(this).addClass("d-flex justify-content-between bg-default mb-3");
        $(this).children().addClass("p-2 flex-fill bg-default");

        //Позиционирование элементов
        $(this).children().css({ "text-align": "center" });
        $(this).children().first().css({ "text-align": "left" });
        $(this).children().last().css({ "text-align": "right" });

        //Удаляем классы
        $(".category .card-body").not(this).children().parent().removeClass("d-flex justify-content-between bg-default mb-3");
        $(".category .card-body").not(this).children().removeClass("p-2 flex-fill bg-default");

        //Восстанавливаем значения полей
        $(".category .card-body").not(this).children().children().css({ "display": "block" });
        $(".category .card-body").not(this).children(".description").css("display", "none");

        $(".category .p-1").not(this.parentNode.parentNode).css({ "width": value });
    });

    let count = 0;
    let cost = 0;
    let discount = 0;
    let result = 0;
    //add into bucket
    $(".add-bucket").click(function () {
        count++;

        let product = $(this.parentNode.parentNode.parentNode.parentNode);

        cost = cost + Number(product.find(".cost").text());
        discount = cost / 10;
        result = cost - discount;

        $(".content-bucket").append(product.html() + "<br>");
        $(".content-bucket").find(".add-bucket").text("X");
        $(".content-bucket").find(".add-bucket").addClass("delete");
        $(".content-bucket").find(".add-bucket").removeClass("add-bucket");

        $(".summ").text(cost);
        $(".discount").text(discount);
        $(".result").text(result);

        $(".badge").text(count);
    });

    //remove out of bucket
    $(".content-bucket").on("click", ".delete", function () {
        let product = $(this.parentNode.parentNode.parentNode);
        cost = cost - Number(product.find(".description .cost").text());
        discount = cost / 10;
        result = cost - discount;
        
        $(product).remove();
        count--;
        $(".summ").text(cost);
        $(".discount").text(discount);
        $(".result").text(result);

        $(".badge").text(count);
    });

});